# Test Front-End Inside

## Introduction

- L'objectif de ce test est de créer une SPA (Single Page Application) en utilisant une API externe.
- Vous devez utiliser le framework Javascript Vue.js (version 3, en composition api). Il est donc recommandé d'utiliser vue-cli.
- Il est recommandé d'utiliser fetch pour récupérer les données https://developer.mozilla.org/fr/docs/Web/API/Fetch_API/Using_Fetch

Seront évalués :

- L'organisation et la propreté du code
- Utilisation des standards ES6 +
- L'organisation des css

## Consignes

### Étape 1

- Reproduire globalement la maquette fournie. (Les maquettes étant fournies en jpg, ce n'est pas un souci si ce n'est pas exactement la même chose)
- Gérer le responsive afin que le site soit présentable sur mobile

### Étape 2

- Afficher la liste des utilisateurs
- Pouvoir ajouter un utilisateur
- Pouvoir supprimer un utilisateur

### Étape 3

- Pouvoir trier les utilisateurs par "nom" dans l'ordre ascendant et descendant en cliquant sur le titre de la colonne

### Optionnel

- Gérer le responsive de la page

## Rendu

Le rendu doit se faire sur un repo privé github/bitbucket/gitlab/etc avec un accès pour technique@maecia.com

## Ressources

- API
  - GET : https://600962ab0a54690017fc3233.mockapi.io/api/v1/users , permet de récupérer la liste des utilisateurs
  - POST : https://600962ab0a54690017fc3233.mockapi.io/api/v1/users , permet d'ajouter un utilisateur (un objet avec seulement le name suffit, c'est pour cette raison qu'il n'y a qu'un champ sur la maquette, le reste est généré automatiquement)
  - DELETE : https://600962ab0a54690017fc3233.mockapi.io/api/v1/users/<id de l'utilisateur>
- La maquette (le logo peut être remplacé par n'importe quelle autre image)
- Les pictos utilisés se trouvent dans le dossier /resources
- Font: Lato (Google Fonts)
